package stepDefination;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;


@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources", glue= {"stepDefination"},plugin={"json:target/cucumber_reports/cucumber.json",
"html:target/cucumber_reports/report.html"})
public class TestRunner {

}
