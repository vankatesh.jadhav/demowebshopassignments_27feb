package pomclasses;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class GiftCard {
	
	WebDriver driver;
	
	@FindBy(partialLinkText="Gift Cards")
	private WebElement clickgiftcards;
	
	@FindBy(id="products-pagesize")
	private WebElement selectgiftcard;
	
	@FindBy(xpath="//a[text()='$5 Virtual Gift Card']")
	private WebElement displaycartdetails;
	

	public GiftCard(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
		this.driver=driver;
	}
	
	public void clickGiftCard()
	{
		clickgiftcards.click();
	}
	
	public void selectGiftCard()
	{
		Select sl = new Select(selectgiftcard);
		sl.selectByVisibleText("4");
	}
	
	public void displayGiftCart()
	{
		System.out.println(displaycartdetails.getText());
	}

}
